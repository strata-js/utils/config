# Strata Config Utility

Configuration loading utility that supports json, and yaml configuration files and environmental substitutions.

## Getting Started

```typescript
import configUtil from '@strata-js/util-config';

interface MyConfig
{
    someSetting : string;
    nestedConfig : {
        host : string;
        port : number;
    }
}

// Load config file. (This will typically happen as early as possible.)
configUtil.load('./config.yml');

// Get config. (This will happen everywhere you need to access the config.)
const config = configUtil.get<MyConfig>();
```

## API

### `load(filePath : string, name : string, options : boolean) : void`

* `filePath : string` - The location of the config file to load. If not provided, it will use the `CONFIG_FILE` 
  environment variable.
* `name : string` - A name to assign to this config. Allows for multiple config files to be loaded. Defaults to 
  `'default'`.
* `options : ConfigLoaderOptions` - Options to pass to the config loader. See below for details.

### `ConfigLoaderOptions`
This interface defines the options that can be passed to the load method of the ConfigUtil class.  

* `substituteEnvironmentVariables : boolean` (optional): A boolean value that indicates whether to substitute environment variables 
  in the configuration file content. If this property is set to true, any environment variables in the configuration 
  file content are substituted with their corresponding values. If this property is not specified, 
  the default behavior is `true`.
* `mergeIncludes : boolean` (optional): A boolean value that indicates whether to merge included configuration files into the 
  main configuration file. If this property is set to true, any configuration files specified in the include property 
  of the main configuration file are merged into the main configuration file. If this property is not specified, 
  the default behavior is `true`.

Parses the config file specified by `filePath`. Once parsed, the config is made available under `name`, or the 
default value of `'default'`. 

### `get<T>(name ?: string) : T`

* `name : string` - The name assigned to the config during load. If not passed, defaults to `'default'`.

Loads the config specified by `name`, if supplied, otherwise loads the config under `'default'`'.

### `set<T>(config : T, name ?: string) : void`

* `config : T` - The config to set.
* `name : string` - The name to assign to the config. If not passed, defaults to `'default'`.

Sets the config specified by `name`, if supplied, otherwise sets the config under `'default'`'.

### `delete(name ?: string) : void`

* `name : string` - The name assigned to the config during load. If not passed, defaults to `'default'`.

Deletes the config specified by `name`, if supplied, otherwise clears the config under `'default'`'.

### `clear() : void`

Clears all configs.

