// ---------------------------------------------------------------------------------------------------------------------
// Unit Tests for the index.spec.ts module.
// ---------------------------------------------------------------------------------------------------------------------

import { hostname } from 'node:os';
import { expect } from 'chai';

import { ConfigUtil } from '../src/index.js';

// ---------------------------------------------------------------------------------------------------------------------

let configUtil : ConfigUtil = new ConfigUtil();

// ---------------------------------------------------------------------------------------------------------------------

describe('Strata ConfigUtil Tests', () =>
{
    beforeEach(() =>
    {
        configUtil = new ConfigUtil();
    });

    afterEach(() =>
    {
        configUtil.clear();
    });

    describe('Basic Functionality Tests', () =>
    {
        it('should throw an error if no config has been loaded', () =>
        {
            expect(() => configUtil.get()).to.throw('Config named \'default\' not found.');
        });

        it('should throw and error if no config of the specified name has been loaded', () =>
        {
            expect(() => configUtil.get('test')).to.throw('Config named \'test\' not found.');
        });

        it('should load config from the filepath parameter', () =>
        {
            configUtil.load('./test/config/standard.yml');
            const config = configUtil.get();

            expect(config).to.not.be.undefined;
            expect(config).to.be.an('object');
            expect(config).to.have.property('stringProp', 'test');
        });

        it('should support loading more than one file under different names', () =>
        {
            configUtil.load('./test/config/standard.yml', 'ConfigA');
            configUtil.load('./test/config/standard.yml', 'ConfigB');
            const configA = configUtil.get('ConfigA');
            const configB = configUtil.get('ConfigB');

            expect(configA).to.not.be.undefined;
            expect(configA).to.be.an('object');
            expect(configA).to.have.property('stringProp', 'test');

            expect(configB).to.not.be.undefined;
            expect(configB).to.be.an('object');
            expect(configB).to.have.property('stringProp', 'test');
        });

        it('should support setting config directly', () =>
        {
            const config = { test: 'test' };
            configUtil.set(config);
            const result = configUtil.get();

            expect(result).to.not.be.undefined;
            expect(result).to.be.an('object');
            expect(result).to.have.property('test', 'test');
        });

        it('should support setting config directly under a name', () =>
        {
            const config = { test: 'test' };
            configUtil.set(config, 'test');
            const result = configUtil.get('test');

            expect(result).to.not.be.undefined;
            expect(result).to.be.an('object');
            expect(result).to.have.property('test', 'test');
        });

        it('should support setting config under multiple names', () =>
        {
            const config = { test: 'test' };
            configUtil.set(config, 'testA');
            configUtil.set(config, 'testB');
            const resultA = configUtil.get('testA');
            const resultB = configUtil.get('testB');

            expect(resultA).to.not.be.undefined;
            expect(resultA).to.be.an('object');
            expect(resultA).to.have.property('test', 'test');

            expect(resultB).to.not.be.undefined;
            expect(resultB).to.be.an('object');
            expect(resultB).to.have.property('test', 'test');
        });

        it('should support deleting configs', () =>
        {
            configUtil.load('./test/config/standard.yml', 'ConfigA');
            configUtil.load('./test/config/standard.yml', 'ConfigB');
            configUtil.delete('ConfigA');

            expect(() => configUtil.get('ConfigA')).to.throw('Config named \'ConfigA\' not found.');
            expect(() => configUtil.get('ConfigB')).to.not.throw();
        });

        it('should support clearing all configs', () =>
        {
            configUtil.load('./test/config/standard.yml', 'ConfigA');
            configUtil.load('./test/config/standard.yml', 'ConfigB');
            configUtil.clear();

            expect(() => configUtil.get('ConfigA')).to.throw('Config named \'ConfigA\' not found.');
            expect(() => configUtil.get('ConfigB')).to.throw('Config named \'ConfigB\' not found.');
        });
    });

    describe('Environment Variable Substitution Tests', () =>
    {
        it('should substitute environment variables', () =>
        {
            process.env.ENV1 = 'cats';
            configUtil.load('./test/config/envtest.yml');
            delete process.env.ENV1;
            const config = configUtil.get();

            expect(config).to.not.be.undefined;
            expect(config).to.have.property('noSub', 'test');
            expect(config).to.have.property('sub', 'cats');
        });

        it('should NOT substitute environment variables', () =>
        {
            process.env.ENV1 = 'cats';
            configUtil.load('./test/config/envtest.yml', 'default', {
                substituteEnvironmentVariables: false
            });
            delete process.env.ENV1;
            const config = configUtil.get();

            expect(config).to.not.be.undefined;
            expect(config).to.have.property('noSub', 'test');
            expect(config).to.have.property('sub', '${ENV1}');
        });
    });

    describe('ENV Defaults Tests', () =>
    {
        it('should change HOSTNAME', () =>
        {
            delete process.env.HOSTNAME;
            // TODO: Do we want this to be part of the library? I'm not sure...
            process.env.HOSTNAME = hostname();
            configUtil.load('./test/config/hostnametest.yml', 'default');
            const config = configUtil.get();

            expect(config).to.not.be.undefined;
            expect(config).to.have.property('hostname', hostname());
        });

        it('should NOT change HOSTNAME', () =>
        {
            const fakeHostname = 'computer0';
            process.env.HOSTNAME = fakeHostname;
            configUtil.load('./test/config/hostnametest.yml', 'default');
            const config = configUtil.get();

            expect(config).to.not.be.undefined;
            expect(config).to.have.property('hostname', fakeHostname);
        });
    });

    describe('JSON Support Tests', () =>
    {
        it('should load and set a default config from the filepath parameter', () =>
        {
            configUtil.load('./test/config/standard.json');
            const config = configUtil.get();

            expect(config).to.not.be.undefined;
            expect(config).to.be.an('object');
        });

        it('should load a named config from the filepath parameter', () =>
        {
            configUtil.load('./test/config/standard.json', 'ConfigA');
            configUtil.load('./test/config/standard.json', 'ConfigB');
            const configA = configUtil.get('ConfigA');
            const configB = configUtil.get('ConfigB');

            expect(configA).to.not.be.undefined;
            expect(configA).to.be.an('object');
            expect(configB).to.not.be.undefined;
            expect(configB).to.be.an('object');
        });

        it('supports env substitution', () =>
        {
            process.env.ENV1 = 'cats';
            configUtil.load('./test/config/envtest.json');
            delete process.env.ENV1;
            const config = configUtil.get();

            expect(config).to.not.be.undefined;
            expect(config).to.have.property('noSub', 'test');
            expect(config).to.have.property('sub', 'cats');
        });
    });

    describe('YAML Support Tests', () =>
    {
        it('should load and set a default config from the filepath parameter', () =>
        {
            configUtil.load('./test/config/standard.yml');
            const config = configUtil.get();

            expect(config).to.not.be.undefined;
            expect(config).to.be.an('object');
        });

        it('should load a named config from the filepath parameter', () =>
        {
            configUtil.load('./test/config/standard.yml', 'ConfigA');
            configUtil.load('./test/config/standard.yml', 'ConfigB');
            const configA = configUtil.get('ConfigA');
            const configB = configUtil.get('ConfigB');

            expect(configA).to.not.be.undefined;
            expect(configA).to.be.an('object');
            expect(configB).to.not.be.undefined;
            expect(configB).to.be.an('object');
        });

        it('supports env substitution', () =>
        {
            process.env.ENV1 = 'cats';
            configUtil.load('./test/config/envtest.yml');
            delete process.env.ENV1;
            const config = configUtil.get();

            expect(config).to.not.be.undefined;
            expect(config).to.have.property('noSub', 'test');
            expect(config).to.have.property('sub', 'cats');
        });
    });

    describe('Merge Includes Tests', () =>
    {
        it('should merge included files', () =>
        {
            configUtil.load('./test/config/includeMergeA.yml');
            const config = configUtil.get<any>();

            expect(config).to.not.be.undefined;
            expect(config).to.be.an('object');
            expect(config).to.have.property('objA');
            expect(config['objA']).to.have.property('propA', 'mergePropA');
            expect(config['objA']).to.have.property('propB', 'propB');
        });

        it('should recursively merge included files', () =>
        {
            configUtil.load('./test/config/includeMergeAB.yml');
            const config = configUtil.get<any>();

            expect(config).to.not.be.undefined;
            expect(config).to.be.an('object');
            expect(config).to.have.property('objA');
            expect(config['objA']).to.have.property('propA', 'mergePropAB');
            expect(config['objA']).to.have.property('propB', 'propB');
            expect(config).to.have.property('objB');
            expect(config['objB']).to.have.property('propB', 'mergePropB');
        });

        it('should merge multiple included files with subsequent values overriding previous values', () =>
        {
            process.env.INCLUDE_DEPTH = '1';
            configUtil.load('./test/config/include3.yml');
            const config = configUtil.get<any>();

            expect(config).to.not.be.undefined;
            expect(config).to.be.an('object');
            expect(config).to.have.property('objA');
            expect(config['objA']).to.have.property('propA', 'mergePropAB');
            expect(config['objA']).to.have.property('propB', 'propB');
            expect(config).to.have.property('objB');
            expect(config['objB']).to.have.property('propB', 'mergePropB');
            expect(config['objB']).to.have.property('propC', 'mergePropC');
        });

        it('should fail if the include depth reaches the max', () =>
        {
            process.env.INCLUDE_DEPTH = '1';
            expect(() =>
            {
                configUtil.load('./test/config/includeMergeAB.yml');
            }).to.throw(Error);
            process.env.INCLUDE_DEPTH = undefined;
        });

        it('should NOT merge included files', () =>
        {
            configUtil.load('./test/config/includeMergeA.yml', 'default', { mergeIncludes: false });
            const config = configUtil.get<any>();

            expect(config).to.not.be.undefined;
            expect(config).to.be.an('object');
            expect(config).to.have.property('objA');
            expect(config['objA']).to.have.property('propA', 'mergePropA');
            expect(config['objA']).to.not.have.property('propB', 'propB');
        });
    });
});

// ---------------------------------------------------------------------------------------------------------------------
