//----------------------------------------------------------------------------------------------------------------------
// A utility for environment variable substitution
// This is copied from the @tuplo/envsubst project, because the package is broken on npm and not well maintained.
//----------------------------------------------------------------------------------------------------------------------

const varNames = '[a-zA-Z_]+[a-zA-Z0-9_]*';
const placeholders = [ '\\$_', '\\${_}', '{{_}}' ];
const envVars = placeholders
    .map((placeholder) => placeholder.replace('_', `(${ varNames })`))
    .join('|');
const rgEnvVars = new RegExp(envVars, 'gm');

//----------------------------------------------------------------------------------------------------------------------

function _includeVariable(shellFormat : string | undefined, varName : string) : boolean
{
    return (typeof shellFormat === 'undefined' || shellFormat.indexOf(varName) > -1);
}

export function substitute(input : string, shellFormat ?: string) : string
{
    const match = input.matchAll(rgEnvVars);
    if(!match) { return input; }

    return Array.from(match)
        .map((item) =>
        {
            const [ varInput, varName ] = item
                .slice(0, placeholders.length + 1)
                .filter(Boolean);

            const value
                = typeof process.env[varName] === 'undefined'
                    ? varInput
                    : process.env[varName];

            return [ varInput, value ];
        })
        .filter(([ varInput ]) => varInput && _includeVariable(shellFormat, varInput))
        .reduce(
            (acc, [ varInput = '', value = '' ]) => acc.replace(varInput, value),
            input
        );
}

//----------------------------------------------------------------------------------------------------------------------
