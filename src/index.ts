// ---------------------------------------------------------------------------------------------------------------------
// Config Util
// ---------------------------------------------------------------------------------------------------------------------

import { FileLoaderOptions, loadFile } from './engines/fileLoader.js';

// ---------------------------------------------------------------------------------------------------------------------

interface ConfigLoaderOptions 
{
    substituteEnvironmentVariables ?: boolean;
    mergeIncludes ?: boolean;
}

const defaultFileLoaderConfig : FileLoaderOptions = {
    substituteEnvironmentVariables: true,
    mergeIncludes: true,
};

export class ConfigUtil
{
    #configRecords = new Map<string, Record<string, unknown>>();

    // -----------------------------------------------------------------------------------------------------------------
    // Public API
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Load a config file into the config store.
     *
     * @param filePath - The path to the config file to load.
     * @param name - The name to give the config. (Defaults to 'default')
     * @param options - Options to use when loading the config file.
     */
    load(filePath : string, name = 'default', options ?: ConfigLoaderOptions) : void
    {
        const effectiveOptions = { ...defaultFileLoaderConfig, ...options } as FileLoaderOptions;
        const config = loadFile(filePath, effectiveOptions);
        this.#configRecords.set(name, config);
    }

    /**
     * Set a config record directly.
     *
     * @param config - The config record to set.
     * @param name - The name to give the config. (Defaults to 'default')
     */
    set<T extends Record<string, unknown>>(config : T, name = 'default') : void
    {
        this.#configRecords.set(name, config);
    }

    /**
     * Get a config record by name.
     *
     * @param name - The name of the config to get. (Defaults to 'default')
     *
     * @returns Returns the config record.
     */
    get<T>(name = 'default') : T
    {
        const config = this.#configRecords.get(name);
        if(!config)
        {
            throw new Error(`Config named '${ name }' not found.`);
        }

        return config as T;
    }

    /**
     * Delete a config record by name.
     *
     * @param name - The name of the config to delete. (Defaults to 'default')
     */
    delete(name = 'default') : void
    {
        this.#configRecords.delete(name);
    }

    /**
     * Clear all config records.
     */
    clear() : void
    {
        this.#configRecords.clear();
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new ConfigUtil();

// ---------------------------------------------------------------------------------------------------------------------
