// ---------------------------------------------------------------------------------------------------------------------
// File Loader Engine
// ---------------------------------------------------------------------------------------------------------------------

import { dirname, extname, resolve } from 'node:path';
import { readFileSync } from 'node:fs';
import * as yaml from 'yaml';
import lodash from 'lodash';

// Utils
import { substitute } from '../utils/environment.js';

// ---------------------------------------------------------------------------------------------------------------------

export interface FileLoaderOptions
{
    substituteEnvironmentVariables : boolean;
    mergeIncludes : boolean;
}

// ---------------------------------------------------------------------------------------------------------------------

function _loadFileString(filePath : string, substituteEnvVars : boolean) : string
{
    const resolvedFilePath = resolve(filePath);
    let fileContents = readFileSync(resolvedFilePath, 'utf8');

    if(substituteEnvVars)
    {
        fileContents = substitute(fileContents);
    }

    return fileContents;
}

function _parseFileString(fileContents : string, filePath : string) : Record<string, unknown>
{
    const fileExtension = extname(filePath).toLowerCase();

    switch (fileExtension)
    {
        case '.yaml':
        case '.yml':
            return yaml.parse(fileContents);
        case '.json':
            return JSON.parse(fileContents);
        default:
            throw new Error(`Unsupported file type: ${ fileExtension }`);
    }
}

function _mergeIncludeFiles(
    baseFilePath : string,
    baseObj : Record<string, unknown>,
    includes : string[],
    options : FileLoaderOptions,
    level = 0
) : Record<string, unknown>
{
    let resultObj : Record<string, unknown> | undefined;
    for(const includeFile of includes)
    {
        let depth = level;
        const includeFilePath = resolve(dirname(baseFilePath), includeFile);
        // eslint-disable-next-line no-use-before-define
        const includeObj = loadFile(includeFilePath, options, ++depth);
        if(!resultObj)
        {
            resultObj = includeObj;
        }
        lodash.merge(resultObj, includeObj);
    }
    lodash.merge(resultObj, baseObj);
    return resultObj as Record<string, unknown>;
}

// ---------------------------------------------------------------------------------------------------------------------

export function loadFile(filePath : string, options : FileLoaderOptions, level = 0) : Record<string, unknown>
{
    // Do not want this to end up in an infinite loop of recursion
    const maxDepth = parseInt(process.env.INCLUDE_DEPTH as string)
        ? parseInt(process.env.INCLUDE_DEPTH as string) : 10;
    if(level > maxDepth)
    {
        throw Error(`include does not support nested includes deeper than ${ maxDepth } levels, `
            + `use the INCLUDE_DEPTH environment variable to change this maximum`);
    }
    const fileContents = _loadFileString(filePath, options.substituteEnvironmentVariables);
    const fileObj = _parseFileString(fileContents, filePath);

    if(options.mergeIncludes && fileObj['include'])
    {
        const includeFiles = Array.isArray(fileObj['include']) ? fileObj['include'] : [ fileObj['include'] ];
        delete fileObj['include'];
        return _mergeIncludeFiles(filePath, fileObj, includeFiles as string[], options, level);
    }

    return fileObj;
}

// ---------------------------------------------------------------------------------------------------------------------
